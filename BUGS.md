#Bug 1:
###Game freezes when user mouses over rabbit

#####Details:
- The game freezes when the user mouses over rabbit
	- Could be due to the game taking too long to calculate routes
		- Therefore, efficiency problem
		- Could also be drawing issue (with the move prediction), but unlikely due to other characters not sharing the same problem
				
- Might have something to do with an additional bug (After performing this bug, the rabbit can be seen moving in between turns

#####Logs/Images/Video:

Game Freezing [https://i.gyazo.com/3f961bc2c8525a7d431fb9c2ccf222f5.gif]

Rabbit moving mid turn [https://i.gyazo.com/797eaae8d25befb0500d901641b3a2dd.gif]

#####Reproduction Rate: 
100%	
#####Steps on how to reproduce: 
Mouse over rabbit during your turn

#Bug 2: 
###Wolf movement does not match path prediction


#####Details:
- Path prediction shows the quicket path, working as intended
- Wolf does not always take this path	
	- Seems to resolve itself if the wolf path UI gizmo is drawn twice
		
#####Logs/Images/Video:
Wolf not taking path predicted: [https://i.gyazo.com/dfcc905db552c60c66de4dad57fe9fe5.gif]

#####Reproduction rate: 
Sometimes, with conditions needing to be met (drawn once/0 times between moves)
- 1/4 chance to move correctly, likely
	 
#####Steps on how to reproduce: 
- Play/Move normally. 
- Only hover over wolf once per turn
