import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.*;

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected RabbitAdapter rabbit;
    private List<Character> allCharacters;
    protected Player player;
    public int hovering = 0;
    public Character currentHover;
    static Character currentPath;
    static List<RelativeMove> pathing;
    private Instant timeOfLastMove = Instant.now();
    private static Stage instance = new Stage();

    public static Stage getInstance() {
        return instance;
    }

    private Stage() {
        SAWReader sr = new SAWReader("data/stage1.saw");
        grid = new Grid(10, 10);
        shepherd = new Shepherd(grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new MoveTowards(shepherd));
        wolf = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new MoveTowards(sheep));
        rabbit = new RabbitAdapter(grid.getRandomCell());

        player = new Player(grid.getRandomCell());
        this.register(player);
        player.startMove();

        // put in the blocks we found in the config file
        sr.getBlocks().forEach(p -> grid.blockCell(p.first, p.second));


        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
        allCharacters.add(rabbit);

    }

    public void update() {
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                allCharacters.forEach((c) -> {
                            //Create a new Thread "charmove"
                            Thread characterMove = new Thread() {
                                //Run the thread (running)
                                public void run() {
                                    List<RelativeMove> moves = c.aiMoves();
                                    moves.get(0).perform();
                                }
                            };
                            //start, then quickly interrupt the thread
                            //Charmove thread should only run the instruction once
                            characterMove.start();
                            characterMove.interrupt();
                        }
                );
                player.startMove();
                timeOfLastMove = Instant.now();
            }
        }
    }

    public static void showPath(List<RelativeMove> lst) {
        lst.forEach(rm -> {
            if (rm instanceof MoveUp) {
                System.out.print("^");
            } else if (rm instanceof MoveDown) {
                System.out.print("\\/");
            } else if (rm instanceof MoveRight) {
                System.out.print("->");
            } else if (rm instanceof MoveLeft) {
                System.out.print("<-");
            }
        });
        System.out.println("");
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        rabbit.paint(g);
        player.paint(g);

        for (Character c : allCharacters) {
            if (c.getLocationOf().contains(mouseLocation)) {
                //new path
                Path here = new Path(c);

                //Default
                if (currentPath == null && !here.isAlive() && pathing == null && currentHover == null) {
                    currentHover = c;
                    here.start();
                    here.interrupt();

                    //current path =/= pathing
                } else if (currentPath == c && pathing != null && !here.isAlive()) {
                    Cell originalLoc = c.getLocationOf();
                    //List<RelativeMove> moves = c.aiMoves();
                    System.out.print("possible path:");
                    showPath(pathing);

                    //Assuming the path is just correct (lol)
                    for (RelativeMove m : pathing) {
                        m.perform();
                        c.getLocationOf().paintOverlay(g);
                    }
                    c.setLocationOf(originalLoc);
                    //draw path
                }
                //If new object has mouse hover
                else if (currentHover != c) {
                    pathing = null;
                    currentPath = null;
                    currentHover = null;
                }
            }
            //If object that draws path no longer has mouse hover, null everything
            else if (currentHover == c) {
                pathing = null;
                currentPath = null;
                currentHover = null;
            }
        }
    }

    // momento implmentation
    public static class Momento {
        private Cell wolfLoc;
        private Cell shepLoc;
        private Cell sheepLoc;
        private Cell rabbitLoc;
        private Cell playerLoc;
        private Behaviour wolfBeh;
        private Behaviour shepBeh;
        private Behaviour sheepBeh;

        public Momento(Stage s) {
            wolfLoc = s.wolf.location;
            shepLoc = s.shepherd.location;
            sheepLoc = s.sheep.location;
            rabbitLoc = s.rabbit.location;
            playerLoc = s.player.location;
            wolfBeh = s.wolf.behaviour;
            shepBeh = s.shepherd.behaviour;
            sheepBeh = s.sheep.behaviour;
        }
    }

    public Momento getMomento() {
        return new Momento(this);
    }

    public void restoreMomento(Momento m) {
        wolf.location = m.wolfLoc;
        sheep.location = m.sheepLoc;
        shepherd.location = m.shepLoc;
        player.location = m.playerLoc;
        wolf.behaviour = m.wolfBeh;
        shepherd.behaviour = m.sheepBeh;
        sheep.behaviour = m.sheepBeh;
    }

}