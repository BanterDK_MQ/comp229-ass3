import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.*;

//Basic Thread/runnable extension
public class Path extends Thread implements Runnable {
    Character description;
    Cell location;
    List<RelativeMove> moves;

    public Path(Character wolf) {
        this.description = wolf;
    }

    public void run() {
        List<RelativeMove> moves = description.aiMoves();
        Stage.pathing = moves;
        Stage.currentPath = description;
    }
}
